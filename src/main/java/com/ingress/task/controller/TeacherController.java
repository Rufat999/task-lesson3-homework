package com.ingress.task.controller;


import com.ingress.task.entity.Teacher;
import com.ingress.task.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/teacher")
public class TeacherController {

    public final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping(path = "/all")
    public List<Teacher> getAllTeachers(){
        return teacherService.getAllTeachers();
    }

    @GetMapping(path = "/{id}")
    public Teacher getTeacherById(@PathVariable Integer id){
        return teacherService.getTeacherById(id);
    }

    @PostMapping(path = "/create")
    public void create(@RequestBody Teacher teacher){
        teacherService.create(teacher);
    }

    @PutMapping(path = "/update/{id}")
    public void update(@PathVariable Integer id, @RequestBody Teacher teacher){
        teacherService.update(id, teacher);
    }

    @DeleteMapping(path = "/delete/{id}")
    public void deleteById(@PathVariable Integer id){
        teacherService.deleteById(id);
    }

    @DeleteMapping(path = "/delete/all")
    public void deleteAll(){
        teacherService.deleteAll();
    }
}
package com.ingress.task.service;

import com.ingress.task.entity.Teacher;
import com.ingress.task.repository.TeacherRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    public final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }


    public List<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }

    public Teacher getTeacherById(Integer id) {
        return teacherRepository.findById(id).get();
    }

    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public void update(Integer id, Teacher teacher) {
        Teacher updateTeacher = teacherRepository.findById(id).get();

        updateTeacher.setName(teacher.getName());
        updateTeacher.setFaculty(teacher.getFaculty());
        updateTeacher.setBirthdate(teacher.getBirthdate());
        teacherRepository.save(updateTeacher);
    }

    public void deleteById(Integer id) {
        teacherRepository.deleteById(id);
    }

    public void deleteAll() {
        teacherRepository.deleteAll();
    }
}
